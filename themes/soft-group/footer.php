            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ( ! dynamic_sidebar('footer') ) : ?>
                        <?php endif; ?>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </footer>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>