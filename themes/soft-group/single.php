<?php get_header(); ?>


    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">
            <?php get_template_part( 'loop' ); ?>
            <!-- Second Blog Post -->
        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">
            <?php get_sidebar('right'); ?>
        </div>
    </div>

<?php get_footer(); ?>