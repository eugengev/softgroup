<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <!-- First Blog Post -->
        <h2>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h2>
        <p class="lead">
            <?=__('by', 'sg'); ?> <a href="<?php the_author_link(); ?>"><?php the_author(); ?></a>
        </p>
        <p><span class="glyphicon glyphicon-time"></span> <?=__('Posted on', 'sg'); ?> <?php the_date(); the_time(); ?></p>
        <hr>
        <?php if (has_post_thumbnail()) { ?>
            <?php the_post_thumbnail('p900x300',array(
                'class' => "img-responsive",
                'alt' => trim(strip_tags( $wp_postmeta->_wp_attachment_image_alt ))
            )); ?>
            <hr>
        <?php } ?>
        <p><?php the_excerpt(); ?></p>
        <a class="btn btn-primary" href="<?php the_permalink(); ?>"> <?=__('Read More', 'sg');?><span class="glyphicon glyphicon-chevron-right"></span></a>
        <hr>
    <?php endwhile; ?>
<?php endif; ?>
<?php
    if(function_exists('wp_paginate')):
        wp_paginate();
    else :
        the_posts_pagination( array(
            'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'sg' ) . '</span>',
            'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'sg' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'sg' ) . ' </span>',
        ) );
    endif;
?>