<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
        <input type="text" value="" name="s" id="s" class="form-control">
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit" name="submit" id="searchsubmit">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
</form>

