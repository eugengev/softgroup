<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('sg', get_template_directory() . '/languages');


/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favico.png" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');


/**
 * Реєструємо місце для меню
 */

register_nav_menus( array(
    'primary' => __('Primary', 'sg'),
) );


/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
    register_sidebars(1, array(
        'id' => 'left',
        'name' => __('Left sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>'
    ));
    register_sidebars(1, array(
        'id' => 'right',
        'name' => __('Right sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
    register_sidebars(1, array(
        'id' => 'footer',
        'name' => __('footer sidebar', 'sg'),
        'description' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
}


/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 );
}
/**
 * Можемо добавити різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) {
    //add_image_size( 'photo', 148, 148, true );
    add_image_size( 'p900x300', 900, 300, true );
}


/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );


/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 200;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
    wp_enqueue_script("jquery");
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_script('init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/**
 * Підключаємо css файли
 */
function add_theme_style(){
    //wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'blog-home', get_template_directory_uri() . '/css/blog-home.css');
    wp_enqueue_style( 'blog-post', get_template_directory_uri() . '/css/blog-post.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

add_action('test', 'test');
function test( $a ){
    return '1';
}

add_filter('test', 'test2');
function test2( $a ){
    return '2';
}

class MyFirstWidget extends WP_Widget {
    public function __construct() {
        parent::__construct("text_widget", "My First Widget",
            array("description" => "A simple widget to show how WP Plugins work"));
    }
    public function form($instance) {
        $title = "";
        $countShow = "5";
        $showTagsCategory = "";

        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $title = $instance["title"];
            $countShow = $instance["countShow"];
            $showTagsCategory = $instance["showTagsCategory"];
        }
        $list_taxonomies = get_taxonomies(['show_ui' => true, 'show_in_nav_menus' => true, '_builtin' => true], 'object');

        $tableId = $this->get_field_id("title");
        $tableName = $this->get_field_name("title");
        echo '<div><label for="' . $tableId . '">'.__('Title').'</label><br>';
        echo '<input class="widefat" id="' . $tableId . '" type="text" name="' .
            $tableName . '" value="' . $title . '"></div>';


        $showTagsCategoryId = $this->get_field_id("showTagsCategory");
        $showTagsCategoryName = $this->get_field_name("showTagsCategory");
        echo '<div><label for="' . $showTagsCategoryId . '">'.__('Taxonomy').'</label><br>';
        echo '<select class="widefat" id="' . $showTagsCategoryId . '" name="' . $showTagsCategoryName .'">';
        foreach ($list_taxonomies as $key => $value) {
            echo '<option '.($showTagsCategory == $key ? 'selected' : '' ).' value="'.$key.'">'.$value->label.'</option>';
        }
        echo '</select></div>';


        $countShowId = $this->get_field_id("countShow");
        $countShowName = $this->get_field_name("countShow");
        echo '<div><label for="' . $countShowId . '">'.__('Show Count').'</label><br>';
        echo '<input type="number" min="2" class="widefat" id="' . $countShowId . '" name="' . $countShowName .'" value="'.$countShow.'" /></div>';

        echo '<br>';
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values["countShow"] = htmlentities($newInstance["countShow"]);
        $values["showTagsCategory"] = htmlentities($newInstance["showTagsCategory"]);
        return $values;
    }

    public function widget($args, $instance) {
        $title = $instance["title"];
        $countShow = $instance["countShow"];
        $showTagsCategory = $instance["showTagsCategory"];
        echo $args['before_widget'];
        echo $args['before_title'].$title.$args['after_title'];
        $argsTerms = array(
            'taxonomy' => $showTagsCategory,
            'hide_empty'  => 0,
            'number'     => $countShow,
            'orderby'     => 'name',
            'order'       => 'ASC',
        );

        $arTerms = get_terms( $argsTerms );

        if( $arTerms && ! is_wp_error($arTerms) ){
            echo '<div class="row">';
            $count = 0;
            foreach ($arTerms as $term) {
                $count++;
                echo '<div class="col-xs-6"><a href="'.get_term_link( $term ).'">'.$term->name.'</a></div>';
                if ($count == 2) {
                    echo '</div><div class="row">';
                    $count = 0;
                }
            };
            echo '</div>';
        }

        echo $args['after_widget'];
    }

}

add_action("widgets_init", function () {
    register_widget("MyFirstWidget");
});