<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                <?php bloginfo('name'); ?>
                <small><?php bloginfo('description'); ?></small>
            </h1>

            <?php get_template_part( 'templates/loop', 'home' ); ?>
            <!-- Second Blog Post -->

        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">
            <?php get_sidebar('right'); ?>
        </div>
    </div>

<?php get_footer(); ?>